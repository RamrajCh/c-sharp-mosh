namespace Class
{
    public class Person
    {
        public string FName;
        public string LName;

        public void Introduce()
        {
            Console.WriteLine($"My name is {FName} {LName}");
        }
    }
}
