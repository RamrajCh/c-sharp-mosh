﻿using Class.Math;

namespace Class
{
    class Program{
        static void Main(string[] args)
        {
            var hari = new Person();
            hari.FName = "Hari";
            hari.LName = "Bahadur";
            hari.Introduce();

            var num = Calculator.Add(5,6);
            System.Console.WriteLine(num);
        }
    }
}
