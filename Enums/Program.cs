﻿using System;

namespace Enums
{
    public enum ShippingMethod
            {
                Regular,
                Registered,
                Express
            }

    class Program
    {
        static void Main(string[] args)
        {
            var method = ShippingMethod.Express;
            System.Console.WriteLine((int)method);

            var methodId = 1;
            System.Console.WriteLine((ShippingMethod)methodId);

            var methodName = "Regular";
            System.Console.WriteLine(Enum.Parse(typeof(ShippingMethod), methodName));
        }
    }
}
