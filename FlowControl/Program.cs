﻿using System;

namespace FlowControl
{
    class Program
    {
        public static void Main(string[] args)
        {
            //1- Write a program to count how many numbers between 1 and 100 are divisible by 3 with no remainder. Display the count on the console.
            // var count = 0;
            // for (var i=0; i<100; i++)
            // {
            //     if (i % 3 == 0) count++;
            // }
            // System.Console.WriteLine($"Count: {count}");
            
            //2- Write a program and continuously ask the user to enter a number or "ok" to exit. Calculate the sum of all the previously entered numbers and display it on the console.
            // int sum = 0;
            // do
            // {
            //     Console.WriteLine("enter a number. enter \"ok\" to exit.");
            //     var number = Console.ReadLine();
            //     if (number != "ok") sum += Convert.ToInt32(number);
            //     else break;

            // } while (true);
            // System.Console.WriteLine($"Sum: {sum}");

            // 3- Write a program and ask the user to enter a number. Compute the factorial of the number and print it on the console. For example, if the user enters 5, the program should calculate 5 x 4 x 3 x 2 x 1 and display it as 5! = 120.
            // Console.WriteLine("enter a number");
            // var number = Convert.ToInt32(Console.ReadLine());
            // var factorial = 1;
            // while(number > 0)
            // {
            //     factorial *= number;
            //     number--;
            // }
            // System.Console.WriteLine($"Factorial: {factorial}");

            // 4- Write a program that picks a random number between 1 and 10. Give the user 4 chances to guess the number. If the user guesses the number, display “You won"; otherwise, display “You lost". (To make sure the program is behaving correctly, you can display the secret number on the console first.)
            // var rand = new Random();
            // var number = rand.Next(1,10);
            // for (var i=0; i<4; i++)
            // {
            //     System.Console.WriteLine("Guess a number");
            //     var guess = Convert.ToInt32(System.Console.ReadLine());
                
            //     if (number == guess)
            //     {
            //         System.Console.WriteLine("You win");
            //         break;
            //     }
            //     if (i == 3)
            //     {
            //         System.Console.WriteLine("You Lose");
            //     }
            // }

            // 5- Write a program and ask the user to enter a series of numbers separated by comma. Find the maximum of the numbers and display it on the console. For example, if the user enters “5, 3, 8, 1, 4", the program should display 8.
            System.Console.WriteLine("Enter a series.");
            var series = System.Console.ReadLine();
            var numbers = series.Split(',');

            var max = Convert.ToInt16(numbers[0]);

            for(var i=1; i<numbers.Length; i++)
            {
                var num = Convert.ToInt16(numbers[i]);
                if (num > max) max = num;
            }
            System.Console.WriteLine($"Greatest:{max}");

        }
    }
}