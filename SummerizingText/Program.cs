﻿using System;
using System.Collections.Generic;

namespace SummerizingText
{
    class Program
    {
        static void Main(string[] args)
        {
            var sentence = @"This ia a long sentence. This ia a long sentence. This ia a long sentence. This ia a long sentence. 
This ia a long sentence. This ia a long sentence. This ia a long sentence. This ia a long sentence.";
            
            var summary = StringUtility.Summerizer(sentence, 100);
            System.Console.WriteLine(summary);
        }
    }
}