namespace SummerizingText
{
    public class StringUtility
    {
        public static string Summerizer(string sentence, int threshold=20)
        {
            if (sentence.Length >= threshold)
            {
                var words = sentence.Split(' ');
                var totalChar = 0;
                var summaryWords = new List<string>();

                foreach (var word in words)
                {
                    summaryWords.Add(word);

                    totalChar += word.Length + 1;
                    if (totalChar > threshold) break;
                }

                return String.Join(' ', summaryWords) + "...";
            }
            else
            {
                return sentence;                
            }
        }
    }
}