﻿using System;

namespace TypeConversion
{
    class Program
    {
        static void Main(string[] args){
            try
            {
                var number = "false";
                bool a = Convert.ToBoolean(number);
                System.Console.WriteLine(a);
            }
            catch (System.OverflowException)
            {
                System.Console.WriteLine("The number couldnot be converted to a byte.");
            }
            
        }
    }
}