﻿using System;

namespace Variables {
    class Program {
        static void Main(string[] args){
            var number = 25;
            int count = 9;
            var total = 49.5; 
            var character = 'A';
            string fileName = "Variables";
            bool isTrue = false;
            Console.WriteLine(total);
            Console.WriteLine(count);
            System.Console.WriteLine(number);
            System.Console.WriteLine(character);
            System.Console.WriteLine(fileName);
            System.Console.WriteLine(isTrue);
        }
    }
}